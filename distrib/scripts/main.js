$(document).ready(function () {

    // MAP GOOGLE
    $('#map_canvas').gmap().bind('init', function (ev, map) {
        $('#map_canvas').gmap('addMarker', {'position': '57.7973333,12.0502107', 'bounds': true}).click(function () {
            $('#map_canvas').gmap('openInfoWindow', {'content': ''}, this);
        });
    });

    // CIRLCE STATS
    $(".dial").knob();

    // SUB-MENU
    $('.sub-dropdown').click(function () {
        $(this).find('.arrow').toggleClass('active');
        $(this).find('.sub-dropdown-menu').toggleClass('active');
        return false;
    })

    // SELECT-ALL
    $('#check-all, #check-all-2').click(function () {
        $('#check-all, .one-row .label-check input[type="checkbox"], .bottom .label-check input[type="checkbox"]').not(this).prop('checked', this.checked);
    });

    $('select').chosen({disable_search_threshold: 10});

    // CHECK IF MODAL
    $('.modal').on('shown.bs.modal', function (e) {
        $(this).find('select').chosen('destroy').chosen({disable_search_threshold: 10});
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('select').chosen('destroy').chosen({disable_search_threshold: 10});
    })

    // FILTER
    Filter = {
        List: $('.filter-list'),
        Header: $('.filter-list .header'),
        Overlay: '<div class="overlay hidden-lg"></div>',

        Init: function () {
            Filter.Header.on('click', function () {
                if ($(window).width() < 1180) {
                    if (Filter.List.hasClass('active')) {
                        Filter.List.removeClass('active');
                        $('.overlay').remove();
                    }
                    else {
                        Filter.List.addClass('active');
                        $('body').prepend(Filter.Overlay);
                        Filter.List.find('select').chosen('destroy').chosen({disable_search_threshold: 10});
                    }
                }
            })
        }
    }

    // TABS MOBILE
    TabMobile = {
        List: $('.nav-tabs'),
        Code: '',

        Init: function () {
            if ($(window).width() < 752) {
                $('.mobile-tabs').remove();
                TabMobile.List.addClass('disactive');
                TabMobile.Run();
                $('#mobile-select').on('change', function (e) {
                    TabMobile.List.find('li a').eq($(this).val()).tab('show');
                });
            } else {
                $('.mobile-tabs').remove();
            }
        },

        Run: function () {
            var getHref;
            TabMobile.Code = '<select id="mobile-select">';
            //TabMobile.List.find('li').length;
            TabMobile.List.find('li').each(function (index) {
                TabMobile.Code += '<option value="' + index + '">' + $(this).find('a').text() + '</option>';
            });
            TabMobile.Code += '</select>';
            TabMobile.List.parent().prepend('<div class="mobile-tabs"></div>');
            $('.mobile-tabs').html(TabMobile.Code);
        }
    }

    $(window).resize(function () {
        TabMobile.Init();
    });

    Filter.Init();
    TabMobile.Init();

});