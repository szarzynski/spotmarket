$(document).ready(function () {

    // MAP GOOGLE
    $('#map_canvas').gmap().bind('init', function (ev, map) {
        $('#map_canvas').gmap('addMarker', {'position': '57.7973333,12.0502107', 'bounds': true}).click(function () {
            $('#map_canvas').gmap('openInfoWindow', {'content': ''}, this);
        });
    });

    // CIRLCE STATS
    $(".dial").knob();

    // SUB-MENU

    function expandMenu() {
      if ($(window).width() < 1180) {
          $('nav').find('.dropdown-menu').addClass('expand');
      } else {
          $('nav').find('.dropdown-menu').removeClass('expand');
      }
    }

    $('.sub-dropdown > a').click(function () {
        $(this).parent().find('.arrow').toggleClass('active');
        $(this).parent().find('.sub-dropdown-menu').toggleClass('active');
        return false;
    });
    $('.sub-dropdown .sub-dropdown-menu a').click(function () {
        setTimeout(function() {
            TabFromMenu.Init();
        }, 50);
    });

    // TAB FROM MAIN MENU
    TabFromMenu =  {
        Init: function() {
            // Javascript to enable link to tab
            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
            }

            // Change hash for page-reload
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                window.location.hash = e.target.hash;
                Scrollbars.Update();
            });
        }
    }

    // SELECT-ALL
    $('#check-all, #check-all-2').click(function () {
        $('#check-all, .one-row .label-check input[type="checkbox"], .bottom .label-check input[type="checkbox"]').not(this).prop('checked', this.checked);
    });

    // DATE PICKERS
    $('.input-group').datetimepicker({
        language: 'pl'
    });

    // DROPDOWNS
    $('select').chosen({disable_search_threshold: 10});

    // CHECK IF MODAL
    $('.modal').on('shown.bs.modal', function (e) {
        $(this).find('select').chosen('destroy').chosen({disable_search_threshold: 10});
        Grading.Reset();
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('select').chosen('destroy').chosen({disable_search_threshold: 10});
    });

    // FILTER
    Filter = {
        List: $('.filter-list'),
        Header: $('.filter-list .header'),
        Overlay: '<div class="overlay hidden-lg"></div>',

        Init: function () {
            Filter.Header.on('click', function () {
                if ($(window).width() < 1180) {
                    if (Filter.List.hasClass('active')) {
                        Filter.List.removeClass('active');
                        $('.overlay').remove();
                    }
                    else {
                        Filter.List.addClass('active');
                        $('body').prepend(Filter.Overlay);
                        Filter.List.find('select').chosen('destroy').chosen({disable_search_threshold: 10});
                    }
                }
            })
        }
    };

    // TABS MOBILE
    TabMobile = {
        List: $('.tabs:not(".precedent")').find('.nav-tabs'),
        ListP: $('.tabs.precedent').find('.nav-tabs').first(),
        Code: '', CodeP: '',

        Init: function () {
            if ($(window).width() < 1179) {
                TabMobile.ListP.addClass('disactive');
                TabMobile.RunP();
                $('#mobile-select-precedent').on('change', function (e) {
                    TabMobile.ListP.find('li a').eq($(this).val()).tab('show');
                });
            } else {
                $('.mobile-tabs-precedent').remove();
                TabMobile.ListP.removeClass('disactive');
            }
            if ($(window).width() < 752) {
                if ($('.mobile-select').length == 0) {
                TabMobile.List.each(function() {
                    TabMobile.Code = '';
                    $(this).addClass('disactive');
                    if ($(this).parent().find('.mobile-select')) {
                        TabMobile.Code = '<select class="mobile-select" style="width: 260px">';
                        $(this).find('li').each(function (index) {
                            TabMobile.Code += '<option value="' + index + '">' + $(this).find('a').text() + '</option>';
                        });
                        TabMobile.Code += '</select>';
                        $(this).parent().prepend('<div class="mobile-tabs"></div>');
                        $(this).parent().find('.mobile-tabs').html(TabMobile.Code);
                        $(this).parent().find('.mobile-select').chosen('destroy').chosen({disable_search_threshold: 10});
                    }
                });
                    $('.mobile-select').on('change', function (e) {
                        $(this).parent().parent().find('.nav-tabs').find('li a').eq($(this).val()).tab('show');
                    });
                }
            } else {
                $('.mobile-tabs').remove();
                TabMobile.List.removeClass('disactive');
            }
        },


        RunP: function () {
            if ($('#mobile-select-precedent').length == 0) {
                TabMobile.CodeP = '<select id="mobile-select-precedent" style="width: 300px">';
                TabMobile.ListP.find('li').each(function (index) {
                    TabMobile.CodeP += '<option value="' + index + '">' + $(this).find('a').text() + '</option>';
                });
                TabMobile.CodeP += '</select>';
                TabMobile.ListP.parent().prepend('<div class="mobile-tabs-precedent"></div>');
                $('.mobile-tabs-precedent').html(TabMobile.CodeP);
                $('#mobile-select-precedent').chosen('destroy').chosen({disable_search_threshold: 10});
            }
        }
    };

    Scrollbars = {

        Statue: $('#statues .wrapper'),
        Message: $('.talk .inside'),

        Init: function() {
            Scrollbars.Statue.mCustomScrollbar({
                mouseWheel: true,
                autoHideScrollbar: false,
                contentTouchScroll: true,
                scrollButtons: {
                    enable: false
                },
                advanced: {
                    updateOnBrowserResize: true
                }
            });
            Scrollbars.Message.mCustomScrollbar({
                mouseWheel: true,
                autoHideScrollbar: false,
                contentTouchScroll: true,
                scrollButtons: {
                    enable: false
                },
                advanced: {
                    updateOnBrowserResize: true
                }
            });
        },

        Update: function() {
            Scrollbars.Statue.mCustomScrollbar('update');
            Scrollbars.Message.mCustomScrollbar('update');
        }
    };

    Accordio = {
      Block: $('.accordio'),

      Init: function() {
        Accordio.Block.find('.title').click(function() {
            if($(this).parent().is('.active')) {
                $(this).parent().find('.body').stop().slideUp(500, function() {
                    $(this).parent().removeClass('active');
                });
            } else {
                $(this).parent().find('.body').stop().slideDown(500, function() {
                    $(this).parent().addClass('active');
                });

            }
        })
      }
    };

    Grading = {
      Platform: $('.give-grade'),

        Init: function() {
            Grading.Platform.find('.pull-right').each(function(y) {
              elems = $(this).find('i');
              elems.each(function(index) {
                 $(this).on("click", function() {
                     Grading.Stars(index, y);
                 });
              });
              console.log(elems);
            });
        },
        Stars: function(ind, pind) {
            console.log('INDEX:' + ind + ', ' + pind);
            choosenPull = Grading.Platform.find('.pull-right:eq('+pind+')');
            choosenPull.find('i').removeClass('star star_gold');
            for($i=0; $i <= ind; $i++) {
              choosenPull.find('i:eq('+ $i +')').addClass('star_gold');
            }
            choosenPull.find('i:not(.star_gold)').addClass('star');
            choosenPull.find('strong').html(ind+1 + '.0');
            choosenPull.find('input[type="text"]').val(ind+1);
        },
        Reset: function() {
            Grading.Platform.find('.pull-right').each(function() {
                $(this).find('input').val('');
                $(this).find('strong').html('1.0')
                //$(this).find('i:not(:eq(0))').removeClass('star_gold');
                relems = $(this).find('i');
                relems.each(function(index) {
                    if (index != 0) {
                        if ($(this).has('star_gold')) {
                            $(this).addClass('star').removeClass('star_gold');
                        }
                    }
                });
            });
        }
    };

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        Scrollbars.Update();
    });


    $(window).resize(function () {
        TabMobile.Init();
        expandMenu();
    });

    Filter.Init();
    TabMobile.Init();
    TabFromMenu.Init();
    Scrollbars.Init();
    Accordio.Init();
    Grading.Init();
    expandMenu();

 //   Scrollbar.Init();
});